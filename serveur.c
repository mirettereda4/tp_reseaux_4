/******************************************************************************/
/*			Application: ....			              */
/******************************************************************************/
/*									      */
/*			 programme  SERVEUR 				      */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs :  ....						      */
/*		Date :  ....						      */
/*									      */
/******************************************************************************/	

#include<stdio.h>
#include <curses.h>

#include<sys/signal.h>
#include<sys/wait.h>
#include<stdlib.h>

#include "fon.h"     		/* Primitives de la boite a outils */
#include <string.h>   
#include "game_functions.c"

#define SERVICE_DEFAUT "1111"

void serveur_appli (char *service);   /* programme serveur */


/******************************************************************************/	
/*---------------- programme serveur ------------------------------*/

int main(int argc,char *argv[])
{

	char *service= SERVICE_DEFAUT; /* numero de service par defaut */


	/* Permet de passer un nombre de parametre variable a l'executable */
	switch (argc)
 	{
   	case 1:
		  printf("defaut service = %s\n", service);
		  		  break;
 	case 2:
		  service=argv[1];
            break;

   	default :
		  printf("Usage:serveur service (nom ou port) \n");
		  exit(1);
 	}

	/* service est le service (ou numero de port) auquel sera affecte
	ce serveur*/
	
	serveur_appli(service);
}

/******************************************************************************/	
void serveur_appli(char *service)
{
	
/* Procedure correspondant au traitemnt du serveur de votre application */

	// Création du socket.

	int id =h_socket(AF_INET ,SOCK_STREAM);
        
	struct sockaddr_in *p_adr_socket=(struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
	adr_socket (service, "127.0.0.1", SOCK_STREAM, &p_adr_socket);
	h_bind(id,p_adr_socket);

	// Le serveur initialisé se place en écoute (attente de requête client).

	h_listen(id,5);
	struct sockaddr_in *p_adr_client;
	int id2=h_accept(id,p_adr_client);
	char*string =(char*)malloc(100*sizeof(char));
	h_reads (id2,string ,9);

	// Début du jeu.

	if (strcmp(string,"commencer")==0){

		// Le serveur demande au client de saisir un niveau pour le jeu.

		h_writes (id2,"veuillez entrer le niveau du jeu !! de 15 à 99 ",49);

		// Le serveur recoit la reponse du client.

		h_reads (id2,string ,2);
		int NB_ESSAIS=atoi(string);

		// Le serveur tire un mot secret du dictionnaire de mots et annonce au client le nombre de lettres du mot secret.

		char MOT_CHOISI[100]={0};
		if (choisirMot(MOT_CHOISI)){
		         int tailleMot = strlen(MOT_CHOISI);
                int *lettreTrouvee = malloc(tailleMot * sizeof(int));
                for (int i = 0 ; i < tailleMot ; i++)
       			 { lettreTrouvee[i]=0;}
                  char buffer[200]={};
				 
				  sprintf(buffer, "la taille du mot proposé est %d", tailleMot);
				   h_writes (id2,buffer,50);
				   char t[3];
				sprintf(t, "%d", tailleMot);
				h_writes (id2,t,3);

	         // Pour chaque caractère proposé par le client, le serveur retourne pour chaque essai une chaîne représentant l'état de découverte du mot 
                 // secret (les caractères déjà trouvés à leur bonne position, et des tirets montrant les caractères non trouvés) et le nombre d’essais 
                 // restant à chaque tentative.
			 
		    while  (NB_ESSAIS!=0 && !victoire(lettreTrouvee,tailleMot)){
			

					sprintf(buffer ," Il vous reste %d coups a jouer : Profitez en bien \nQuel est le mot secret ? ", NB_ESSAIS);
					h_writes (id2,buffer,78);
					h_reads (id2,string ,100);
					//extraire le caractére lu 
					char LU=string[0];
					printf("%c\n", LU);
					//vérifier si la lettre lu est une bonne lettre 
					bonneLettre(LU,MOT_CHOISI,lettreTrouvee);
					printf("%s\n",MOT_CHOISI);
					printf("\n\n");
					char buffer_mot[tailleMot];
					printf("les lettres trouvées sont : {");
					//remplissage du buffer à envoyer au client comme résultat de la saisie
					for (int i = 0 ; i < tailleMot ; i++)
						{
						if (lettreTrouvee[i]) {                                      
						printf("  %c  ",MOT_CHOISI[i]);
						buffer_mot[i]=MOT_CHOISI[i];
						}else {buffer_mot[i]='-';}

				    }
					printf(" }\n");
			    NB_ESSAIS--; 
				h_writes(id2,buffer_mot,tailleMot);
			}//fin while
			
		free(lettreTrouvee);
		
		}//fin if mot choisi

		// Fermeture de la session de communication ouverte entre le client et le serveur apres la fin du jeu (lorsque toutes les lettres du mot 
                // secret ont été trouvées ou lorsque le nombre d'essais restant est nul).

		h_close(id2);
	}//fin if commencer
	
}//fin fonction
/******************************************************************************/