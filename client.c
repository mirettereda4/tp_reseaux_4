/******************************************************************************/
/*			Application: ...					*/
/******************************************************************************/
/*									      */
/*			 programme  CLIENT				      */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs : ... 					*/
/*									      */
/******************************************************************************/	


#include <stdio.h>
#include <ctype.h>
#include <curses.h> 		/* Primitives de gestion d'ecran */
#include <sys/signal.h>
#include <sys/wait.h>
#include<stdlib.h>

#include "fon.h"   		/* primitives de la boite a outils */
#include <string.h>   

#define SERVICE_DEFAUT "1111"
#define SERVEUR_DEFAUT "127.0.0.1"

void client_appli (char *serveur, char *service);
void upper(char* s){
for (int i = 0; s[i]!='\0'; i++) {
      toupper(s[i]);
   }
}
/****************************************************************************/
/*--------------- programme client -----------------------*/

int main(int argc, char *argv[])
{

	char *serveur= SERVEUR_DEFAUT; /* serveur par defaut */
	char *service= SERVICE_DEFAUT; /* numero de service par defaut (no de port) */


	/* Permet de passer un nombre de parametre variable a l'executable */
	switch(argc)
	{
 	case 1 :		/* arguments par defaut */
		  printf("serveur par defaut: %s\n",serveur);
		  printf("service par defaut: %s\n",service);
		  break;
  	case 2 :		/* serveur renseigne  */
		  serveur=argv[1];
		  printf("service par defaut: %s\n",service);
		  break;
  	case 3 :		/* serveur, service renseignes */
		  serveur=argv[1];
		  service=argv[2];
		  break;
    default:
		  printf("Usage:client serveur(nom ou @IP)  service (nom ou port) \n");
		  exit(1);
	}

	/* serveur est le nom (ou l'adresse IP) auquel le client va acceder */
	/* service le numero de port sur le serveur correspondant au  */
	/* service desire par le client */
	
	client_appli(serveur,service);
}

/*****************************************************************************/
void client_appli (char *serveur,char *service)

/* procedure correspondant au traitement du client de votre application */


{
	//la creation d'une socket 
	int id =h_socket(AF_INET ,SOCK_STREAM);
	// allocatin dans la memoire de la structure adresse du socket du client
	struct sockaddr_in *p_adr_socket=(struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
	
	
	// la fonction adr_socket remplie les champs de la structure sockaddr_in par le numero de port et 
	//l'adresse ip "4.4.4.1" du client et elle fait le bind , sock_stream signifie qu'on utilise le mode tcp
	adr_socket ("0", "4.4.4.1", SOCK_STREAM, &p_adr_socket);
	
	
        //allocation dans la memoire de la structure adresse du socket du serveur
	struct sockaddr_in *p_adr_serveur=(struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
	
	
	//la fonction adr_socket rempli les champs de la structure sockadrr_in par le numero de port et de l'adresse
	// ip du serveur , sock_stream signifie qu'on utilise le mode tcp
	adr_socket (service,serveur, SOCK_STREAM, &p_adr_serveur);
	
	
	printf(" si vous voulez commencer le jeu , Tapez \"commencer\",sinon tapez \"quitter\"\n");
	
	//initiliser le tampon de h_writes 
	char buffer[100];
	
	//initialiser la chaine de charactere dans la quelle le client saisis les caracteres 
	char input[100];
	
	
	scanf("%s",input);
	//le client entre un mot qui indique si il veut commencer le jeu ou bien quitter l'application 
	
	
	if (strcmp(input,"quitter")==0){
		// si le client saisi dans input "quitter" alors on va fermer la socket et terminer l'application 
		h_close(id);
		
	}
	else {
		if (strcmp(input,"commencer")==0){
			
			//si le client tape commencer on va etablir une connexion entre le serveur et le client et entrer 
			//dans la boucle du jeu
			h_connect(id,p_adr_serveur);
			int x=h_writes (id,"commencer",9);
			
			
			//ici le serveur envoie au client " il lui demande de saisir le niveau"
			x=h_reads(id,buffer,49);
			printf("%s\n",buffer);
			
			

			do {
				// ici le client saisi le niveau du jeu entre 15 et 99
				scanf("%s",input);
				if ((atoi(input)>100 || atoi(input)<15)){printf("veuillez saisir un nombre entre 15 et 99 svp\n");}
			}while (atoi(input)>100 || atoi(input)<15);
			
			
			// l'application du cote client envoie au serveur le niveau du jeu que le client vient de saisir 
			x=h_writes (id,input,2);
			int NB_ESSAI=atoi(input);
			
			
			//le serveur envoie au client " le mot propse contient "
			x=h_reads(id,buffer,50);
			printf("%s\n",buffer);
			
			
			// le serveur envoie la taille du mot que le client doit deviner 
			x=h_reads(id,buffer,4);
			int TAILLE_MOT=atoi(buffer);
			
			
			//debut game 
			 printf("\033[1;31m");
   			 printf("Bienvenue dans le jeu du Pendu ! Vous avez droit a %d erreurs pour trouver le mot secret ! BONNE CHANCE ...\n", NB_ESSAI);
   			 printf("\033[0m");
   			 
   			 
   			 
			int STATUS=1;
//tant que status =1 , le client n'a ni gagne ni epuise son nombre d'essais
            while(STATUS){
            	//le serveur envoie au client " il vous reste x coups , profiterz en bien , qulle est le mot secret ?
				x=h_reads(id,buffer,77);
			    printf("%s\n",buffer);
			    
			    
				 //saisi lettre
				do{
					scanf("%s",input);
					if (isalpha(input[0])==0 || strlen(input)!=1)
					{
						printf("veuillez saisir une et une lettre  svp\n");
					}else{upper(input);}
					
				}while(isalpha(input[0])==0 || strlen(input)!=1); 
				
				
		// le client envoie la lettre qu'il a devine au serveur 
			    x=h_writes (id,input,sizeof(input));
			    
			    
				//etat d'avancement du jeu
				// le client recoit du serveur le resultat de son essai 
				x=h_reads(id,buffer,TAILLE_MOT+1);
			   for (int i=0;i<TAILLE_MOT;i++){
			   	printf("%c",buffer[i]); 
			   }
			   printf("\n");
               NB_ESSAI--;
               	// si nombre d'essai =0 ou si le client reussi a deviner le mot avant d'epuiser son nombre d'essai alors il gagne et il ferme la connexion avec le serveur avec h_close 
			   if(NB_ESSAI==0){STATUS=0; printf("\033[1;31m"); printf("vous avez perdu \n");printf("\033[0m");}
			   if(strchr(buffer,'-')==NULL){STATUS=0;printf("\033[1;31m");printf("vous avez gagné \n");printf("\033[0m");}
			 }h_close(id);
			 
			
			
		}
	}
return;

	
	
	
	
	



 }
/*****************************************************************************/