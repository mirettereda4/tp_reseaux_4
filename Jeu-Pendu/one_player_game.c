

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <time.h>




void un_joueur()
{
    char lettre;                                                        
    char motSecret[100] = {0};                                          
    int *lettreTrouvee = NULL;                                          
    int coupsRestants = 10;                                             
    int i = 0;                                                          
    int tailleMot = 0;
   
    char lettresDejaProposees[100] = {0};  
    
    printf("\033[1;31m");
    printf("\n\nBienvenue dans le jeu du Pendu ! Vous avez droit a 10 erreurs pour trouver le mot secret ! BONNE CHANCE ...\n\n\n");
    printf("\033[0m");

    if (!choisirMot(motSecret))
        exit(0);

    tailleMot = strlen(motSecret);

    lettreTrouvee = malloc(tailleMot * sizeof(int)); 
                       
    if (lettreTrouvee == NULL)
        exit(0);

    for (i = 0 ; i < tailleMot ; i++)
        lettreTrouvee[i] = 0;

    time_t begin, end;
    time(&begin);
    
    while (coupsRestants > 0 && !victoire(lettreTrouvee, tailleMot))
    {
        printf("\nIl vous reste %d coups a jouer : Profitez en bien ...", coupsRestants);
        printf("\nQuel est le mot secret ? ");
        
        for (i = 0 ; i < tailleMot ; i++)
        {
            if (lettreTrouvee[i]) {                                      
                printf("\033[1;32m");
                printf("%c", motSecret[i]);                              
                printf("\033[0m");
            }
                
            else {
                printf("\033[1;32m");
                printf("-");                                             
                printf("\033[0m");
            }    
        }

        printf("\nProposez une lettre : ");
        
        lettre = lireCaracterePropose();

        while (rechercheLettreProposeeAvant(lettre, lettresDejaProposees) == 1) {
            printf("\033[1;36m");
            printf("\nCette lettre est deja proposee avant, donc proposez une nouvelle lettre : ");
            printf("\033[0m");
            
            lettre = lireCaracterePropose(); 
        }
        
        int length = strlen(lettresDejaProposees);
        lettresDejaProposees[length] = lettre;
        lettresDejaProposees[++length] = '\0';

        if (!bonneLettre(lettre, motSecret, lettreTrouvee))
        {
            coupsRestants--;                                            
            
            printf("\033[1;33m");
            printf("\n\nAttention : la lettre '%c' n'existe pas dans le mot secret !!! \n\n", lettre);
            printf("\033[0m");
            
            switch (coupsRestants){
                            case 9 : pendu9() ; break ;
                            case 8 : pendu8() ; break ;
                            case 7 : pendu7() ; break ;
                            case 6 : pendu6() ; break ;
                            case 5 : pendu5() ; break ;
                            case 4 : pendu4() ; break ;
                            case 3 : pendu3() ; break ;
                            case 2 : pendu2() ; break ;
                            case 1 : pendu1() ; break ;
                            case 0 : pendu0() ; break ;
                            default : ;
                            } 
            
            printf("\033[1;33m");
            printf("\nFaites attention ... Vous etes proche d'etre pendu !!! \n\n\n\n\n");                       
            printf("\033[0m");
        }
        
        else {
            printf("\033[1;32m");
            printf("\nBravo !!! La lettre '%c' est bien presente dans le mot secret, continuez ainsi ...\n\n\n\n\n", lettre);
            printf("\033[0m");
        }
    }

    if (victoire(lettreTrouvee, tailleMot)) {
        printf("\033[1;32m");
        printf("\nFELICITATIONS !!! Vous avez gagne et maintenant vous meritez votre vie ... Le mot secret etait bien : '%s' \n\n", motSecret);
        printf("\033[0m");
        
        time(&end);
        time_t elapsed = end - begin;
        
        printf("\033[1;36m");
        printf("Votre score ( le temps qu'il t'a fallu pour gagner le jeu ) est : %ld secondes\n\n\n\n\n\n\n", elapsed);
        printf("\033[0m");
    }
        
    else {
        printf("\033[1;32m");
        printf("\nDESOLE !!! Vous avez perdu et vous devez faire vos dernieres prieres puisque vous serez pendu ... HAHAHAHAHA !!! Le mot secret etait : '%s' \n\n\n\n\n\n\n", motSecret);
        printf("\033[0m");
    }
    
    free(lettreTrouvee);                                                

}






