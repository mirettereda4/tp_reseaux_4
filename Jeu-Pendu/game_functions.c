

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>




int nombreAleatoire(int nombreMax)
{
    srand(time(NULL));
    
    return (rand() % nombreMax);
}




int choisirMot(char *motChoisi)
{
    FILE* dictionary = NULL;                                                          
    int nombreMots = 0;
    int numMotChoisi = 0;
    int i = 0;
    int caractereLu = 0;
    
    dictionary = fopen("dictionary.txt", "r");                                        

    if (dictionary == NULL)                                                           
    {
        printf("\nImpossible de charger le dictionnaire de mots");
        return 0;                                                               
    }

    do
    {
        caractereLu = fgetc(dictionary);
        
        if (caractereLu == '\n')
            nombreMots++;
            
    } while(caractereLu != EOF);

    numMotChoisi = nombreAleatoire(nombreMots);                                 

    rewind(dictionary);
    
    while (numMotChoisi > 0)
    {
        caractereLu = fgetc(dictionary);
        
        if (caractereLu == '\n')
            numMotChoisi--;
    } 
    
    fgets(motChoisi, 100, dictionary);

    motChoisi[strlen(motChoisi) - 1] = '\0';
    
    fclose(dictionary);

    return 1;                                                                  
}



/*

char lireCaracterePropose()
{
    char caractere;
                                                    
    scanf("%c", &caractere); 
    
    caractere = toupper(caractere);                                           

    while (getchar() != '\n') ;

    return caractere;                                                         
}

*/


int victoire(int lettreTrouvee[], int tailleMot)
{
    int i = 0;
    int joueurGagne = 1;

    for (i = 0 ; i < tailleMot ; i++)
    {
        if (lettreTrouvee[i] == 0)
            joueurGagne = 0;
    }

    return joueurGagne;
}




int bonneLettre(char lettre, char motSecret[], int lettreTrouvee[])
{
    int i = 0;
    int lettrePresente = 0;

    for (i = 0 ; motSecret[i] != '\0' ; i++)
    {
        if (lettre == motSecret[i])                                          
        {
            lettrePresente = 1;                                                 
            lettreTrouvee[i] = 1;                                            
        }
    }

    return lettrePresente;
}




int rechercheLettreProposeeAvant(char lettre, char lettresDejaProposees[])
{
    int i = 0;
    int lettrePresente = 0;

    for (i = 0 ; lettresDejaProposees[i] != '\0' ; i++)
    {
        if (lettre == lettresDejaProposees[i])                              
        {
            lettrePresente = 1;                                             
            break;
        }
    }

    return lettrePresente;
}







