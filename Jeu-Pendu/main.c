

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "game_functions.c"
#include "hangman_draw.c"
#include "one_player_game.c"
#include "two_players_game.c"




void play();
char playAnotherTime();




int main(int argc, char* argv[])
{   
    char r;
    
    printf("\033[1;31m");
    printf("\n\n\n                                                                              Le Jeu du Pendu       \n\n");
    printf("\033[0m");

    play();
    
    r = playAnotherTime();
    
    while (r == 'y') {
          r = playAnotherTime();
    }
    
    return 0;         
}




void play()
{    
     int nb;
     
     printf("\033[1;34m");
     printf("\nVoulez-vous jouer tout seul ? Ecrivez '1' \n");
     printf("Voulez-vous jouer a deux ?    Ecrivez '2' \n\n");
     printf("\033[0m");
    
     scanf("%d", &nb);
     
     while (getchar() != '\n') ;
    
     while (nb != 1 && nb != 2) {
           printf("\033[1;34m");
           printf("\nVoulez-vous jouer tout seul ? Ecrivez '1' \n");
           printf("Voulez-vous jouer a deux ?    Ecrivez '2' \n\n");
           printf("\033[0m");
    
           scanf("%d", &nb);
           
           while (getchar() != '\n') ;
     }

     if (nb == 1)
        un_joueur();
       
     else
        deux_joueurs();  
}



char playAnotherTime() 
{    
     char r;
     
     printf("\033[1;35m");
     printf("Voulez-vous jouer une autre fois ?     - Si oui, pressez sur 'y' \n");
     printf("                                       - Si non, pressez sur 'n' \n\n");
     printf("\033[0m");  
    
     scanf("%c", &r); 
     
     while (getchar() != '\n') ;
     
     while (r != 'y' && r != 'n') {
           printf("\033[1;35m");
           printf("\nVoulez-vous jouer une autre fois ?     - Si oui, pressez sur 'y' \n");
           printf("                                       - Si non, pressez sur 'n' \n\n");
           printf("\033[0m");  
    
           scanf("%c", &r);
           
           while (getchar() != '\n') ;  
     } 
    
     if (r == 'y') 
        play();
       
     else {
        printf("\033[1;31m");
        printf("\n\n\n\nBonne journee ! Au revoir ... \n\n");
        printf("\033[0m"); 
     }
     
     return r;
}






