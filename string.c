
/**
 * @return la longueur de la chaine de caractères, c'est à dire le nombre
 * de caractères qu'elle contient. 
 * Exemple: "" -> length = 0
 *          "abc" -> length = 3
 */
int string_length(char *s) {
char c = *s ;
int len =0;
while (c!=0){
len=len+1;
c=*(s+len);
}
  return len;
}  


/**
 * Copie la second chaîne sur la première.
 * @param s1 est la première chaîne
 * @param s2 est la seconde chaîne
 */
void string_copy(char* s1, char* s2) {
char c = *s2;
while (c!=0){
*s1=*s2;
s1++;
s2++;
c=*s2;
}
*s1='\0';

}

/**
 * Concatène les deux chaînes de caractères, ajoutant la seconde à la fin 
 * de la première
 * @param s1 est la première chaîne
 * @param s2 est la seconde chaîne
 */
void string_concat(char* s1, char* s2) {
int length= string_length(s1);

s1=s1+length ;
while (*s2){
*s1=*s2;
s1++;
s2++;
}
*s1='\0';
}

/**
 * Compare les deux chaînes de caractères
 * @param s1
 * @param s2
 * @return 0 si les deux chaînes sont identiques.
 *         -1 si la chaîne s1 est lexicographiquement inférieur à la chaîne s2
 *         1 si  la chaîne s1 est lexicographiquement supérieur à la chaîne s2
 */
int string_cmp(char*s1, char* s2) {
char c1 = *s1 ;
char c2 = *s2;
while (c1&&c1==c2 ){
c1=*(s1++);
c2=*(s2++);
}
if (c1==c2) return 0 ;
else if (c1<c2 ) return -1;
     else  return 1 ;



  

}

/**
 * Cherche le caractère donné en argument, dans la chaîne donné en argument,
 * à partir de la position donné (offset).
 */
int string_index_of(char *dst, int offset, char ch) {
char *pos = dst+offset ;
int index = offset;
char c = *pos ;
while (c != ch){
pos = pos+1;
c = *pos;
index = index+1;
}

  return index;
}

/**
 * Copie une sous-chaîne de la chaîne source dans la chaîne destination.
 * La sous-chaîne est défini par l'index du premier caractères (offset) 
 * et sa longueur (length). 
 * Cette sous-chaîne sera copiée dans la chaîne destination à l'index
 * donné par l'argument "doff".
 * @param dst: la chaîne destination.02
 * @param doff: l'offset dnas la chaîne destination
 * @param src: la chaîne source
 * @param offset: l'offset de la sous-chaîne à copier.
 * @param length: la loongueur de la sous-chaîne à copier.
 */
void string_substring(char* dst, int doff, char *src, int offset, int length) {



int c = 0;
char *indexdst=dst+doff;
char *indexsrc= src +offset ;
while (c < length ){
*indexdst = *indexsrc ;
indexdst++;
indexsrc++;
c++;

}


























}
